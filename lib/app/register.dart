import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
//import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:form_field_validator/form_field_validator.dart';
//import 'dashboard.dart';
import 'Login_Screen.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text('Vidyodaya'),
              backgroundColor: Colors.blue[900],
              centerTitle: true,
            ),
            body: Center(
                child: RegisterUser()
            )
        )
    );
  }
}
  class RegisterUser extends StatefulWidget {
    RegisterUserState createState() => RegisterUserState();
  }

class RegisterUserState extends State {
  Future<String> futureClasses;

  final _formKey = GlobalKey<FormState>();
  final requiredValidator = RequiredValidator(errorText: 'this field is required');
  // For CircularProgressIndicator.
  bool visible = false ;

  //  Declaring Variables
  String _firstname;
  String _lastname;
  String _username;
  String _password;
  String _class;
  // ignore: deprecated_member_use
  List classes=List();
  String a;
  bool _autoValidate=false;
//  final emailController = TextEditingController();
//  final passwordController = TextEditingController();
  var item;
  // ignore: unused_field
  var _classe;
  var myclass;
  var isLoading = false;

//Fetch Class using API
  // ignore: missing_return
  Future<String> fetchAlbum() async {
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Class/GetClasses'),
    headers:{"Accept":"application/json"});
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
    setState((){
        classes=json.decode(res.body);
    });
  }

//  Post Data for login Check
   _validateInputs() async{

    final form = _formKey.currentState;
    if (form.validate()) {
      setState(() {
        isLoading = true;
      });
      form.save();
//      print('Success Login');
//      print(_username);
//      print(_password);
//      print(_firstname);
//      print(_lastname);
//      print(_class);

//      Map data = {
////        'FirstName': _firstname,
////        "LastName": _lastname,
////        "EmailAddress": _username,
////        "MobileNumber":_password,
////        "Class":_class
////      };

      // ignore: unused_local_variable
      String url = 'https://brahmiappservice.azurewebsites.net/User/Register';
      Map<String, String> headers = {"Content-type": "application/json", "accept" : "application/json"};
      var jsons = jsonEncode(<String, String>{"FirstName": _firstname, "LastName": _lastname, "EmailAddress": _username,"MobileNumber":_password,"ClassIdentifier":_class});
      print (jsons);
      var response = await http.post('https://brahmiappservice.azurewebsites.net/User/Register', headers: headers, body: jsons);
      setState(() {
        isLoading = false;
      });
      int statusCode = response.statusCode;
      print(statusCode);
      print(response);
      print(response.contentLength);
      print(response.body);
      // ignore: unused_local_variable
      String body = response.body;

      Fluttertoast.showToast(
          msg: "User Registered Successfully",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green[300],
          textColor: Colors.white,
          fontSize: 16.0
      );

//      Navigator.push(
//          context,
//          MaterialPageRoute(
//              builder: (context) => LoginPage()));

    } else {
      print('Not true Validate');
      setState(() => _autoValidate = true);
    }
  }


  @override

  void initState() {
    super.initState();
    futureClasses= fetchAlbum();
    print(futureClasses);
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Center(
              child: Column(
                children: <Widget>[
                  Divider(),
                  Container(
//                    padding: EdgeInsets.all(10),
                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          children: [
                            Image(
                              image:AssetImage('assets/logooo.jpg'),
                              fit: BoxFit.contain,
                              width: 180,
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              'Register!',
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 30),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),

                  Container(
                      child:Form(
                          key: _formKey,
                          // ignore: deprecated_member_use
                          autovalidate: _autoValidate,
                          child:Container(
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                        flex: 1,
                                        child: Container(
                                            padding: EdgeInsets.all(10),
                                            child: TextFormField(
                                              autocorrect: true,
                                              decoration: InputDecoration( border: OutlineInputBorder(),
                                                labelText: 'First Name',
//                                          validator: requiredValidator,
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                  borderSide: BorderSide(color: Colors.grey, width: 2),
                                                ),),
                                              validator: (value) {
                                                if(value.isEmpty)
                                                  return 'Required';
                                                else if (value.length < 3)
                                                  return 'Greater than 2 character';
                                                else
                                                  return null;
                                              },
                                              onSaved: (String value) {
                                                _firstname = value;
                                              },
                                            )
                                        ),
                                    ),

                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          child: TextFormField(
                                            autocorrect: true,
                                            decoration: InputDecoration( border: OutlineInputBorder(),
                                              labelText: 'Last Name',
//                                          validator: requiredValidator,
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                borderSide: BorderSide(color: Colors.grey, width: 2),
                                              ),),
                                            validator: (value) {
                                              if(value.isEmpty)
                                                return 'Required';
                                              else if (value.length < 3)
                                                return 'Greater than 2 character';
                                              else
                                                return null;
                                            },
                                            onSaved: (String value) {
                                              _lastname = value;
                                            },
                                          )
                                      ),
                                    ),
                                  ],
                                ),

                             Row(
                               children: <Widget>[
                                 Expanded(
                                   child:  Container(
                                       padding: EdgeInsets.all(10),
                                       child: DropdownButtonFormField(
                                         items: classes.map((item){
                                           return new DropdownMenuItem(
                                             child:new Text(
                                                item['Name'],
                                               style: TextStyle(
                                                 fontSize: 15.0
                                               ),
                                             ),
                                             value:item['Identifier'].toString(),
                                           );
                                         }).toList(),
                                         decoration: InputDecoration( border: OutlineInputBorder(),
                                           labelText: 'Class',
                                           enabledBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                             borderSide: BorderSide(color: Colors.grey, width: 2),
                                           ),),
                                         onChanged: (String newValue) {
                                           setState(() {
                                             a=newValue;
                                             print(a.toString()) ;
                                           });
                                         },
                                         onSaved: (String item) {
                                           _class = item;
                                         },
                                       ),
                                 )
                                 )
                               ],
                             ),


                            Row(
                              children: <Widget>[
                                Expanded(
                                  child:  Container(
                                      padding: EdgeInsets.all(10),
                                      width: 380,
                                      //                      padding: EdgeInsets.all(10.0),
                                      child: TextFormField(
                                        autocorrect: true,
                                        decoration: InputDecoration( border: OutlineInputBorder(),

                                          labelText: 'User Name',
//                                          validator: requiredValidator,
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                            borderSide: BorderSide(color: Colors.grey, width: 2),
                                          ),),
                                        validator: (value) {
                                          if(value.isEmpty)
                                            return 'Username is Required.';
                                          else if (value.length < 3)
                                            return 'Username must be of Email ';
                                          else
                                            return null;
                                        },
                                        onSaved: (String value) {
                                          _username = value;
                                        },
                                      )
                                  ),
                                )
                              ],
                            ),


                             Row(
                               children: <Widget>[
                                 Expanded(
                                   child:Container(
                                       padding: EdgeInsets.all(10),
                                       width: 380,
                                       child: TextFormField(
                                         autocorrect: true,
                                         decoration: InputDecoration( border: OutlineInputBorder(),
                                           labelText: 'Mobile Number',
                                           enabledBorder: OutlineInputBorder(
                                             borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                             borderSide: BorderSide(color: Colors.grey, width: 2),
                                           ),),
                                         validator: (value) {
                                           if (value.isEmpty)
                                             return 'Password is Required.';
                                           else if(value.length < 10)
                                             return 'Mobile Nuumber must be of 10 digits';
                                           else if(value.length > 10)
                                             return 'Mobile Nuumber must be of 10 digits';
                                           else
                                             return null;
                                         },
                                         onSaved: (String val) {
                                           _password = val;
                                         },
                                       )
                                   ),
                                 )
                               ],
                             ),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    isLoading
                                        ? Center(
                                      child: CircularProgressIndicator(),
                                    ):
                                    Container(
                                        margin: EdgeInsets.only(top: 20),
                                        height: 50,
                                        width:200,
                                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        // ignore: deprecated_member_use
                                        child: RaisedButton(
                                          shape: StadiumBorder(),
                                          textColor: Colors.white,
                                          color: Colors.blue[900],
                                          child: Text('Register'),
                                          onPressed:_validateInputs,
                                        )),

                                  ],

                                )
                              ],
                            ),
                          )
                      )),

                  Container(
                      child: Row(
                        children: <Widget>[
                          Text('Already have account?'),
                          // ignore: deprecated_member_use
                          FlatButton(
                            textColor: Colors.blue,
                            child: Text(
                              'Sign in',
                              style: TextStyle(fontSize: 20),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LoginPage(),
                                ),
                              );
                            },
                          )
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      )),

                  Visibility(
                      visible: visible,
                      child: Container(
                          margin: EdgeInsets.only(bottom: 30),
                          child: CircularProgressIndicator()
                      )
                  ),
                ],
              ),
            )));
      }
    }