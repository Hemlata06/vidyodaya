import 'dart:convert';
//import 'package:filter_list/filter_list.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
//import 'dashboard.dart';
//import 'Menu.dart';
//import 'package:progress_dialog/progress_dialog.dart';
import 'package:form_field_validator/form_field_validator.dart';
//import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.home, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text('PROFILE'),
              backgroundColor: Colors.blue[900],
              centerTitle: true,
            ),
            body: Center(
                child:ProfileUser()
            )
        )
    );
  }
}
class ProfileUser extends StatefulWidget {

  ProfileUserState createState() => ProfileUserState();

}

class ProfileUserState extends State {
  final _formKey = GlobalKey<FormState>();
  final requiredValidator = RequiredValidator(errorText: 'this field is required');

  bool _autoValidate=false;
  var accesstoken;
  // ignore: unused_field
  var _accesstokenq;
  var userIdentifier;
  var _userIdentifierq;
  var isLoading = false;
  // ignore: non_constant_identifier_names
  var UserName;
  // ignore: unused_field
  String _class;


  // ignore: deprecated_member_use
  List classes=List();
  // ignore: deprecated_member_use
  List notes=List();
  // ignore: non_constant_identifier_names
  var UserPrimaryEmailAddress;
  Future<String> futureCall;
  Future<String> futureDetail;
  String firstname;
  var lastname;
  var mobile;
  var email;
  var name='';
  var a;
  var myclass;
//  Fetch Class using API
  // ignore: missing_return
  Future<String> fetchClass() async {
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Class/GetClasses'),
        headers:{"Accept":"application/json"});
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
    setState((){
      classes=json.decode(res.body);
    });
  }

  // ignore: missing_return
  Future<String> fetchUserDetail() async {
    setState(() {
      isLoading = true;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    _userIdentifierq = (prefs.getString('UserIdentifier')??'');

    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/User/Get'), body:{'Identifiers':_userIdentifierq},
      headers:{"charset":"utf-8","Content-Type": "application/x-www-form-urlencoded","Accept":"application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8","Authorization":"m_kH_xlFKbp3wSnZ7TMUJ_dSlw9NixbQN-aWfOjK6R4JzfC1q_GPtf53y_8c4SDpfOXddOp1GEb6Dt9m8Azhm9mD0sPeJysYH_tdWXx7FIOr0_74MUbiXO8fZklAWLlgZ7kebBs823XQFRtklU0q14qygvgvRkebfukZmLMPLMGE8uQInQEkoKXvF7MCyahaRz9QpY8TI03VUoAUwbhkLZGfYXNKOSAiWzfe10GkuJWbMhsHzpuirOmz6lTBADx36FLfWr680Kaogy4wvcarsy7wYtwvl9kIRVZLWMfr8fLeeMNprysgEReBWDmkUaNgK4C-iPz7sbFdnsEQrUP6ShywCMTnAzZ1DWU2dB9wQwo"},
    );

    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body)[0]);
    firstname=json.decode(res.body)[0]['FirstName'];
    print(json.decode(res.body)[0]['Class']['Identifier']);
    setState((){
      name=json.decode(res.body)[0]['FirstName']+' ' +json.decode(res.body)[0]['LastName'];
      firstname=json.decode(res.body)[0]['FirstName'];
      lastname=json.decode(res.body)[0]['LastName'];
      email=json.decode(res.body)[0]['EmailAddress'];
      mobile=json.decode(res.body)[0]['MobileNumber'];
      myclass=json.decode(res.body)[0]['Class']['Identifier'];
    });
    setState(() {
      isLoading = false;
    });
  }


//  Future<String> getUserDetail() async{
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    _accesstokenq = (prefs.getString('accesstoken')??'');
//    print(_accesstokenq);
//
//    _UserIdentifierq = (prefs.getString('UserIdentifier')??'');
//    UserPrimaryEmailAddress = (prefs.getString('UserPrimaryEmailAddress')??'');
//      UserName = (prefs.getString('UserName')??'');
//    print(UserName);
//    print(_UserIdentifierq);
//    print(UserPrimaryEmailAddress);
//
//    setState(() {
//       UserName = (prefs.getString('UserName')??'');
//       UserPrimaryEmailAddress = (prefs.getString('UserPrimaryEmailAddress')??'');
//
//    });
//
//  }

  initState(){
    super.initState();
    futureCall=fetchClass();
    futureDetail= fetchUserDetail();
  }


  @override
  Widget build(BuildContext context) {
    return  isLoading
        ? Center(
      child: CircularProgressIndicator(),
    )
        :Container(
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    color: Colors.lightBlueAccent,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child:Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Column(
                              children: <Widget>[
                                Image(
                                  image:AssetImage('assets/userme.png'),
                                  fit: BoxFit.contain,
                                  width: 150,
                                  height: 150,
                                ),
                                Text(name,style: TextStyle(fontSize: 30,color: Colors.white),),
                              ],
                            )
                          ),
                        )
                      ],
                    ),
                  ),
//                  Container(
//                    padding: EdgeInsets.all(20),
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                      children: [
//                        Column(
//                          children: [
//                            Image(
//                              image:AssetImage('assets/users.png'),
//                              fit: BoxFit.contain,
//                              width: 180,
//                            )
//                          ],
//                        )
//                      ],
//                    ),
//                  ),

                  Container(
                      margin: EdgeInsets.only(top: 30),

                      child:Form(
                          key: _formKey,
                          // ignore: deprecated_member_use
                          autovalidate: _autoValidate,
                          child:Container(
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          child: TextFormField(
                                            autocorrect: true,
                                            initialValue:firstname,
                                            decoration: InputDecoration( border: OutlineInputBorder(),
                                              labelText: 'First Name',
//                                          validator: requiredValidator,
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                borderSide: BorderSide(color: Colors.grey, width: 2),
                                              ),),
                                            validator: (value) {
                                              if(value.isEmpty)
                                                return 'Required';
                                              else if (value.length < 3)
                                                return 'Greater than 2 character';
                                              else
                                                return null;
                                            },
                                            onSaved: (String value) {
                                            },
                                          )
                                      ),
                                    ),

                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                          padding: EdgeInsets.all(10),
                                          child: TextFormField(
                                            autocorrect: true,
                                            initialValue: lastname,
                                            decoration: InputDecoration( border: OutlineInputBorder(),
                                              labelText: 'Last Name',
//                                          validator: requiredValidator,
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                borderSide: BorderSide(color: Colors.grey, width: 2),
                                              ),),
                                            validator: (value) {
                                              if(value.isEmpty)
                                                return 'Required';
                                              else if (value.length < 3)
                                                return 'Greater than 2 character';
                                              else
                                                return null;
                                            },
                                            onSaved: (String value) {
                                            },
                                          )
                                      ),
                                    ),
                                  ],
                                ),

                                Row(
                                  children: <Widget>[
                                    Expanded(
                                        child:Container(
                                          padding: EdgeInsets.all(10.0),
                                          child:  TextFormField(
                                            autocorrect: true,
                                            initialValue:email,
                                            decoration: InputDecoration( border: OutlineInputBorder(),
                                              labelText: 'Email Id',
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                borderSide: BorderSide(color: Colors.grey, width: 2),
                                              ),),
                                            validator: (value) {
                                              if(value.isEmpty)
                                                return 'Password is Required';
                                              else if (value.length < 5)
                                                return 'Password must be more than 5 character';
                                              else
                                                return null;
                                            },
                                            onSaved: (String val) {
                                            },
                                          ),
                                        )
                                    ),

                                  ],
                                ),

                                Row(
                                  children: <Widget>[
                                    Expanded(
                                        child:Container(
                                          padding: EdgeInsets.all(10.0),
                                          child:  TextFormField(
                                            autocorrect: true,
                                            initialValue:mobile,
                                            decoration: InputDecoration( border: OutlineInputBorder(),
                                              labelText: 'Mobile Number',
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                borderSide: BorderSide(color: Colors.grey, width: 2),
                                              ),),
                                            validator: (value) {
                                              if(value.isEmpty)
                                                return 'Password is Required';
                                              else if (value.length < 5)
                                                return 'Password must be more than 5 character';
                                              else
                                                return null;
                                            },
                                            onSaved: (String val) {
                                            },
                                          ),
                                        )
                                    ),

                                  ],
                                ),


                                Row(
                                  children: <Widget>[
                                    Expanded(
                                        child:  Container(
                                          padding: EdgeInsets.all(10),
                                          child: DropdownButtonFormField(
                                            onChanged: (String newValue) {
                                              setState(() {
                                                a=myclass;
                                                print(a.toString()) ;
                                              });
                                            },
                                            items: classes.map((item){
                                              return new DropdownMenuItem(
                                                value:item['Identifier'].toString(),
                                                child:new Text(
                                                  item['Name'],
                                                  style: TextStyle(
                                                      fontSize: 15.0
                                                  ),
                                                ),
                                              );
                                            }).toList(),
                                            decoration: InputDecoration( border: OutlineInputBorder(),
                                              labelText: 'Class',
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                borderSide: BorderSide(color: Colors.grey, width: 2),
                                              ),),

                                            onSaved: (String item) {
                                              _class = item;
                                            },
                                          ),
                                        )
                                    )
                                  ],
                                ),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    // ignore: deprecated_member_use
                                    RaisedButton(
                                      color: Colors.blue[900],
                                      shape: StadiumBorder(),
                                      child: Row(
                                        children: <Widget>[
                                          Text('SAVE',
                                            style: TextStyle(
                                                color: Colors.white
                                            ),),
                                        ],
                                      ),
                                      onPressed: () {},
                                    ),
                                  ],
                                )
                              ],
                            ),
                          )
                      )),



                ],
              ),
            ));
  }}