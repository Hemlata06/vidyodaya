import 'dart:convert';
import 'package:chewie/chewie.dart';
//import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
//import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
//import 'package:dropdown_formfield/dropdown_formfield.dart';
//import 'package:form_field_validator/form_field_validator.dart';
//import 'dashboard.dart';
import 'Menu.dart';
import 'chewie_list_item.dart';
import 'package:video_player/video_player.dart';
//import 'Login_Screen.dart';
import 'package:path_provider/path_provider.dart';
import 'download_provider.dart';
import 'package:provider/provider.dart';
import 'dart:io';
//import 'package:paging/paging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/cupertino.dart';

class SubscribeVideos extends StatefulWidget {
  @override
  _SubscribeVideosState createState() => _SubscribeVideosState();

 // Navigator.pop(context);
}

class _SubscribeVideosState extends State<SubscribeVideos> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text('SUBSCRIBED VIDEOS'),
              backgroundColor: Colors.blue[900],
              centerTitle: true,
            ),
            body: Center(
                child: SubscribeVideosUser()
            )
        )
    );
  }
}
class SubscribeVideosUser extends StatefulWidget {

  SubscribeVideosUserState createState() => SubscribeVideosUserState();

}

class SubscribeVideosUserState extends State 
{

   bool downloading = false;
  var progressString = "";

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;

//  DOWNLOAD FUNCTIONALITY
  _createFolder()async {
    final directory = await getExternalStorageDirectory();
    final folderName = "brahmi";
    final path = Directory("${directory.path}/$folderName");
    if ((await path.exists())) {
      
      print("exist");
    } else {
      print("not exist");
      path.create();
    }
  }
  Future<dynamic> futureNotes;

  TextEditingController _textFieldController = TextEditingController();
  // ignore: unused_field
  String _class;
  // ignore: deprecated_member_use
  List notes=List();
  // ignore: deprecated_member_use
  List videos=List();
  // ignore: non_constant_identifier_names
  var Name;
  var accesstoken;
  // ignore: unused_field
  var _accesstokenq;
  var userIdentifier;
  // ignore: unused_field
  var _userIdentifierq;
  var isLoading = false;
  // ignore: deprecated_member_use
  List classes=List();
  String a;

  //Fetch Class using API
  // ignore: missing_return
  Future<String> fetchAlbum() async {
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Class/GetClasses'),
        headers:{"Accept":"application/json"});
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
    setState((){
      classes=json.decode(res.body);
    });
  }

  Future<String> futureCall;


  Future<dynamic> fetchVideo() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _userIdentifierq = (prefs.getString('UserIdentifier')??'');
    accesstoken = (prefs.getString('accesstoken')??'');
    print(_userIdentifierq);
    print(accesstoken);
    Map<String, dynamic> data={'UserIdentifier':_userIdentifierq,'GetResources':true,'IsLearningPlan':true};
    var bodyEncoded = json.encode(data);
    print(bodyEncoded);
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Video/Get'), body:bodyEncoded,
      headers:{"charset":"utf-8","Content-Type": "application/x-www-form-urlencoded","Accept":"application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8","Authorization":accesstoken},
    );
    setState(() {
      isLoading = false;
    });
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
    setState((){
      videos=json.decode(res.body);
    });
  }


  void initState(){

    WidgetsFlutterBinding.ensureInitialized();
//    await FlutterDownloader.initialize(
//        debug: true // optional: set false to disable printing logs to console
//    );
    super.initState();
    _createFolder();
    futureNotes= fetchVideo();
    print(futureNotes);
    fetchAlbum();
    initializing();
  }

  void initializing() async {
    androidInitializationSettings = AndroidInitializationSettings('app_icon');
    iosInitializationSettings = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = InitializationSettings(
        androidInitializationSettings, iosInitializationSettings);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  // void _showProgressNotifications(retStatus) async{
  //   await progressnotification(retStatus);
  // }
  Future<void> progressnotification(retStatus) async {
    AndroidNotificationDetails androidNotificationDetails =
    AndroidNotificationDetails(
        'Channel ID', 'Channel title', 'channel body',
        priority: Priority.High,
        importance: Importance.Max,
        ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
    NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.show(0, 'Download Video', 'Downloading'+retStatus, notificationDetails);
  }


  // void _showNotifications() async {
  //   await notification();
  // }


  Future<void> notification() async 
  {
    AndroidNotificationDetails androidNotificationDetails =
    AndroidNotificationDetails(
        'Channel ID', 'Channel title', 'channel body',
        priority: Priority.High,
        importance: Importance.Max,
        ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
    NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.show( 0, 'Download Video', 'Your Video  is successfully downloaded', notificationDetails);
  }

  Future<void> notificationAfterSec() async {
    var timeDelayed = DateTime.now().add(Duration(seconds: 10000));
    AndroidNotificationDetails androidNotificationDetails =
    AndroidNotificationDetails('second channel ID', 'second Channel title', 'second channel body',
        priority: Priority.High,
        importance: Importance.Max,
        ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
    NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.schedule(1, 'Hello there',
        'please subscribe my channel', timeDelayed, notificationDetails);
  }

  // ignore: missing_return
  Future onSelectNotification(String payLoad) {
    if (payLoad != null) {
      print(payLoad);
    }

    // we can set navigator to navigate another screen
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: <Widget>[
        CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              print("");
            },
            child: Text("Okay")),
      ],
    );
  }

  void choiceAction(String choice){
    if(choice == Menu.Subscribe){}
  }


  _displayDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Filter Videos'),
          content: Container(
              height: 210.0,
              child:SafeArea(
                  child: Column(
                    children: <Widget>[
                      TextField(
                        controller: _textFieldController,
                        decoration: InputDecoration(hintText: "Subject"),
                      ),

                      SizedBox(
                        height:10.0,
                      ),

                      DropdownButtonFormField(
                        items: classes.map((item){
                          return new DropdownMenuItem(
                            child:new Text(
                              item['Name'],
                              style: TextStyle(
                                  fontSize: 15.0
                              ),
                            ),
                            value:item['Identifier'].toString(),
                          );
                        }).toList(),
                        decoration: InputDecoration(
                          labelText: 'Class',
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            a=newValue;
                            print(a.toString()) ;
                          });
                        },
                        onSaved: (String item) {
                          _class = item;
                        },
                      ),

                      SizedBox(
                        height:25.0,
                      ),

                      TextField(
                        controller: _textFieldController,
                        decoration: InputDecoration(hintText: "Video Title"),
                      ),

                    ],
                  )
              )
          ),
          actions: <Widget>[
            // ignore: deprecated_member_use
            FlatButton( color: Colors.deepPurple,
              child: const Text('APPLY'),
              onPressed: () {
              },
            ),
            // ignore: deprecated_member_use
            FlatButton(
              color: Colors.deepPurple,
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

//  static const String routeName = '/notesPage';

  @override

  Widget build(BuildContext context) {
    var fileDownloaderProvider =
    Provider.of<FileDownloaderProvider>(context, listen: false);
    // ignore: unused_local_variable
    var url='https://www.radiantmediaplayer.com/media/big-buck-bunny-360p.mp4';
//    return Scaffold(
//        backgroundColor: Colors.grey[300],
    return  isLoading
        ? Center(
      child: CircularProgressIndicator(),
    )
        :Container(
        color: Colors.grey[300],

        child:Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  // ignore: deprecated_member_use
                  RaisedButton(
                    color: Colors.deepPurple,
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.filter_list,color: Colors.white),
                        Text('FILTER',
                          style: TextStyle(
                              color: Colors.white
                          ),),
                      ],
                    ),
                    onPressed: () {
                      _displayDialog(context);
                    },
                  ),


                ],
              ),
            ),

            Expanded(
                child:Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child:ListView.builder(
                      itemCount: videos.length,
                      itemBuilder: (BuildContext context,int index){
                        return Card(
                          child: Container(
                            height: 350,
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                             mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 200,
                                  child: ChewieListItem(
                                    videoPlayerController: VideoPlayerController.network(
      '${videos[index]['Resource']['URL']}',
  ),
  looping: false,
 // autoplay: true,
                                    ),
                                ),
                                // Row(
                                //   children: <Widget>[
                                //     Expanded(
                                //       child: ChewieListItem(
                                //         videoPlayerController: VideoPlayerController.network(
                                //           '${videos[index]['Resource']['ThumbnailURL']}',
                                //         ),
                                //       ),
                                //     ),
                                //   ],
                                // ),
//                                  Container(
//                                    color: Colors.white,
//                                    child: Row(
//                                      children: <Widget>[
//                                        Expanded(
//                                          child: Row(
//                                            children: <Widget>[
//                                              Column(
//                                                children: <Widget>[
//                                                  Text('Topic: ${videos[index]['Name']}',
//                                                      style: TextStyle(
//                                                          fontSize: 20.0,
//                                                          fontWeight: FontWeight.bold,
//                                                          color:Colors.grey[800]
//                                                      )),
//                                                  Text('( ${videos[index]['Topic']})',
//                                                      style: TextStyle(
//                                                          fontSize: 15.0,
//                                                          fontWeight: FontWeight.bold,
//                                                          color:Colors.grey[800]
//                                                      )),
//                                                  Text('Cost: ${videos[index]['Cost']}\n',
//                                                      textAlign: TextAlign.center,
//                                                      style: TextStyle(
//                                                        fontSize: 15.0,
//                                                        fontWeight: FontWeight.w300,
//                                                        color:Colors.grey[600],
//                                                      )),
//                                                ],
//                                              ),
//
//
//
//                                            Row(
//                                                children: <Widget>[
//                                                  dowloadButton(fileDownloaderProvider,'${videos[index]['Resource']['ThumbnailURL']}'),
//                                                  downloadProgress(),
//                                                  Container(
//                                                    alignment: Alignment.topRight,
//                                                    child: PopupMenuButton<String>(
//                                                      onSelected: choiceAction,
//                                                      itemBuilder: (BuildContext context){
//                                                        return Menu.choices.map((String choice){
//                                                          return PopupMenuItem<String>(
//                                                            value: choice,
//                                                            child: Text(choice),
//                                                          );
//                                                        }).toList();
//                                                      },
//                                                    ),
//                                                  ),
//                                                ]),
//
//
//                                            ],
//                                          ),
//                                        ),
//                                      ],
//                                    ),
//                                  ),
                                SizedBox(height: 10,),
                                Container(
                                  color:Colors.white,
                                  padding: const EdgeInsets.all(15),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        /*1*/
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            /*2*/
                                            Container(
                                              child: Text(
                                                ' ${videos[index]['Topic']}',
                                                style: TextStyle(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                            Text(
                                              '(${videos[index]['Subject']})',
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Text(
                                              'Cost: ${videos[index]['Cost']}',
                                              style: TextStyle(
                                                color: Colors.black,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      /*3*/
                                      dowloadButton(fileDownloaderProvider,'${videos[index]['Resource']['URL']}','${videos[index]['Name']}'),
                                      downloadProgress(),
//                                    PopupMenuButton<String>(
//                                      onSelected: choiceAction,
//                                      itemBuilder: (BuildContext context){
//                                        return Menu.choices.map((String choice){
//                                          return PopupMenuItem<String>(
//                                            value: choice,
//                                            child: Text(choice),
//                                          );
//                                        }).toList();
//                                      },
//                                    ),
                                    ],
                                  ),
                                ),


                                Divider(color: Colors.grey.shade400, indent:0.0, height: 1.0),



                              ],
                            ),

                          ),
                        );
                      }
                  ),
                )

            )
          ],
        )

    );




  }

  Widget dowloadButton(FileDownloaderProvider downloaderProvider, url, name) {
    // ignore: deprecated_member_use
    return new FlatButton(
      onPressed: () 
      {
        // final imgUrl = url;
    downloadFile(url,"brami","brami");
    
            // downloaderProvider
            //     .downloadFile(url, name+'.exo')
            //     .then((onValue) {});
          },
          textColor: Colors.black,
          child: new Icon(Icons.file_download,color: Colors.black),
        );
      }
    
    
    
      Widget downloadProgress() {
        var fileDownloaderProvider =
        Provider.of<FileDownloaderProvider>(context, listen: true);
    
        return new Text(
          downloadStatus(fileDownloaderProvider),
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        );
      }
    
      downloadStatus(FileDownloaderProvider fileDownloaderProvider) 
      {
        var retStatus = "";
       // var status = "";
        // ignore: unused_local_variable
        Map<String, dynamic> result = {
          'isSuccess': false,
          'filePath': null,
          'error': null,
        };
        // switch (fileDownloaderProvider.downloadStatus) 
        // {
        //   case DownloadStatus.Downloading:
        //     {
        //      status = "Download Progress : " +
        //          fileDownloaderProvider.downloadPercentage.toString() +
        //          "%";
        //      _showProgressNotifications(status);
        //     }
        //     break;
        //   case DownloadStatus.Completed:
        //     {
        //       result['isSuccess'] = 'Success';
        //       // retStatus = 'Completed';
        //       // _showNotifications();
        //     }
        //     break;
        //   case DownloadStatus.NotStarted:
        //     {
        //       retStatus = '';
        //     }
        //     break;
        //   // case DownloadStatus.Started:
        //   //   {
        //   //     retStatus = "";
        //   //   }
        //   //   break;
        // }
    
        return retStatus;
      }


Future<String> downloadFile(String url, String fileName, String dir) async {
        HttpClient httpClient = new HttpClient();
        File file;
        String filePath = '';
        String myUrl = '';
    
        try {
          myUrl = url+'/'+fileName;
          var request = await httpClient.getUrl(Uri.parse(myUrl));
          var response = await request.close();
          if(response.statusCode == 200) {
            var bytes = await consolidateHttpClientResponseBytes(response);
            filePath = '$dir/$fileName';
            file = File(filePath);
            await file.writeAsBytes(bytes);
          }
          else
            filePath = 'Error code: '+response.statusCode.toString();
        }
        catch(ex){
          
          filePath = 'Can not fetch url';
        }
    
        return filePath;
      }
    
    //   Future downloadFile(String url) async 
    //   {
    //      Dio dio = Dio();
    // try {
    //   var dir = await getApplicationDocumentsDirectory();
    //   print("path ${dir.path}");
    //     await dio.download(url, "${dir.path}/demo.mp4",
    //       onReceiveProgress: (rec, total) 
    //       {
    //     print("Rec: $rec , Total: $total");

    //     setState(() {
    //       downloading = true;
    //       progressString = ((rec / total) * 100).toStringAsFixed(0) + "%";
    //     });
    //   });
    // } catch (e) {
    //   print(e);
    // }


      }
