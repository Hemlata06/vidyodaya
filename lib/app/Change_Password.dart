//import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Dashboard.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.home, color: Colors.white),
                onPressed: () =>   Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Dashboard())),
              ),
              title: Text('Change Password'),
              backgroundColor: Colors.blue[900],
              centerTitle: true,
            ),
            body: Center(
                child: ChangePasswordUser()
            )
        )
    );
  }
}
class ChangePasswordUser extends StatefulWidget {
  ChangePasswordUserState createState() => ChangePasswordUserState();
}

class ChangePasswordUserState extends State {

  // ignore: unused_field
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _formKey = GlobalKey<FormState>();
  final requiredValidator = RequiredValidator(errorText: 'this field is required');

  var isLoading = false;
  // ignore: unused_field
  String _password;
  String _oldpassword;
  String _newpassword;
  String _cnewpassword;
  // ignore: non_constant_identifier_names
  var UserPrimaryEmailAddress;

  bool _autoValidate=false;
  // ignore: non_constant_identifier_names
  var error_description;

  void _postdata() async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    UserPrimaryEmailAddress = (prefs.getString('UserPrimaryEmailAddress')??'');
    print(UserPrimaryEmailAddress);

    setState(() {
      isLoading = true;
    });

    final form = _formKey.currentState;

    if (form.validate()) {

      // Text forms was validated.
      form.save();

      if(_newpassword != _cnewpassword) {
        _showMyDialog();
      } else {
        var email=UserPrimaryEmailAddress;
        var response = await http.post(Uri.encodeFull(
            'https://brahmiappservice.azurewebsites.net/User/ChangePassword?userEmailAddress=$email&oldPassword=$_oldpassword&newPassword=$_newpassword'),
            headers: {
              "charset": "utf-8",
              "Content-Type": "application/x-www-form-urlencoded",
              "accept" : "application/json",
              "TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"
            }
        );
        print(response);
        setState(() {
          isLoading = false;
        });
        int statusCode = response.statusCode;
        print(statusCode);
        print(response.contentLength);
        print(response.body);

        if(statusCode==400){
          Fluttertoast.showToast(
              msg: "Not Updated!",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red[300],
              textColor: Colors.white,
              fontSize: 16.0
          );
        }else{
          Fluttertoast.showToast(
              msg: "Password Updated!",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0
          );
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ChangePassword()));
        }

      }

    } else {
      print('Not true Validate');
      setState(() {
        isLoading = false;
      });
      setState(() => _autoValidate = true);
    }

  }


  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert Message'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Password do not match'),
              ],
            ),
          ),
          actions: <Widget>[
            // ignore: deprecated_member_use
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ChangePassword()));
              },
            ),
          ],
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            :SingleChildScrollView(
            child: Center(
                child: Column(
                  children: <Widget>[
                    Form(
                      key: _formKey,
                      // ignore: deprecated_member_use
                      autovalidate: _autoValidate,
                      child: Container(
                        margin: EdgeInsets.only(top: 30),
                        padding: EdgeInsets.all(20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              child: Column(
                                children: [
                                  Text(
                                    'Change Password!',
                                    style: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.w500,
                                        fontSize:25),
                                  ),
                                  Image(
                                    image:AssetImage('assets/key.png'),
                                    fit: BoxFit.contain,
                                    width: 180,
                                  ),

                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                          child:Container(
                                            padding: EdgeInsets.all(10.0),
                                            child:  TextFormField(
                                              autocorrect: true,
                                              obscureText: true,
                                              decoration: InputDecoration( border: OutlineInputBorder(),
                                                labelText: 'Old Password',
//                                          validator: requiredValidator,
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                  borderSide: BorderSide(color: Colors.grey, width: 2),
                                                ),),
                                              validator: (value) {
                                                if(value.isEmpty)
                                                  return 'Old Password is Required';
                                                else if (value.length < 5)
                                                  return 'Password must be more than 5 character';
                                                else
                                                  return null;
                                              },
                                              onSaved: (String val) {
                                                _oldpassword = val;
                                              },
                                            ),
                                          )
                                      ),

                                    ],
                                  ),

                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                          child:Container(
                                            padding: EdgeInsets.all(10.0),
                                            child:  TextFormField(
                                              autocorrect: true,
                                              obscureText: true,
                                              decoration: InputDecoration( border: OutlineInputBorder(),
                                                labelText: 'New Password',
//                                          validator: requiredValidator,
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                  borderSide: BorderSide(color: Colors.grey, width: 2),
                                                ),),
                                              validator: (value) {
                                                if(value.isEmpty)
                                                  return 'New Password is Required';
                                                else if (value.length < 5)
                                                  return 'Password must be more than 5 character';
                                                else
                                                  return null;
                                              },
                                              onSaved: (String val) {
                                                _newpassword = val;
                                              },
                                            ),
                                          )
                                      ),

                                    ],
                                  ),

                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                          child:Container(
                                            padding: EdgeInsets.all(10.0),
                                            child:  TextFormField(
                                              autocorrect: true,
                                              obscureText: true,
                                              decoration: InputDecoration( border: OutlineInputBorder(),
                                                labelText: 'Confirm New Password',
//                                          validator: requiredValidator,
                                                enabledBorder: OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                                  borderSide: BorderSide(color: Colors.grey, width: 2),
                                                ),),
                                              validator: (value) {
                                                if(value.isEmpty)
                                                  return 'New Password is Required';
                                                else if (value.length < 5)
                                                  return 'Password must be more than 5 character';
//                                                else if (value!=_newpassword)
//                                                  return 'Password do not match';
                                                else
                                                  return null;
                                              },
                                            onSaved: (String val) {
                                              _cnewpassword = val;
                                            },
                                            ),
                                          )
                                      ),

                                    ],
                                  ),

                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      isLoading
                                          ? Center(
                                        child: CircularProgressIndicator(),
                                      ):
                                      Container(
                                          margin: EdgeInsets.only(top: 20),
                                          height: 50,
                                          width:200,
                                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                          // ignore: deprecated_member_use
                                          child: RaisedButton(
                                            shape: StadiumBorder(),
                                            textColor: Colors.white,
                                            color: Colors.blue[900],
                                            child: Text('Save'),
                                            onPressed:(_postdata),

                                          )),

                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
            )
        )
    );
  }
}