import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Notes.dart';
import 'Video.dart';
import 'SubscribeNotes.dart';
import 'SubscribeVideos.dart';
import 'Login_Screen.dart';
import 'Profile.dart';
import 'Change_Password.dart';


class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var accesstoken;
  var _accesstokenq;
  var userIdentifier;
  // ignore: unused_field
  var _userIdentifierq;
  var isLoading = false;
  var userName='Unknown';
  var userPrimaryEmailAddress='abc';
  var firstLetter="A";
  Future<String> futureCall;

  // ignore: missing_return
  Future<String> callme() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _accesstokenq = (prefs.getString('accesstoken')??'');
    print(_accesstokenq);

    _userIdentifierq = (prefs.getString('UserIdentifier')??'');
    userPrimaryEmailAddress = (prefs.getString('UserPrimaryEmailAddress')??'');
    userName = (prefs.getString('UserName')??'');
    firstLetter = userName.substring(0,1).toUpperCase();
    print(firstLetter);
    print(userName);
    print(_userIdentifierq);

    setState(() {
      userName = (prefs.getString('UserName')??'');
     firstLetter = userName.substring(0,1).toUpperCase();
    });

  }

    _logout() async{
      setState(() {
        isLoading = true;
      });
      SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.remove('accesstoken');
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => LoginPage()));
    }

   initState(){
    print(accesstoken);
    super.initState();
    futureCall= callme();
  }

  @override
  Widget build(BuildContext context) {
    return  isLoading
        ? Center(
      child: CircularProgressIndicator(),
    )
        :Scaffold(
      appBar: AppBar(
        title: Text('Vidyodaya'),
        centerTitle: true,
        backgroundColor: Colors.blue[900],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                    color: Colors.blue[900], shape: BoxShape.rectangle),
              accountName: Text(userName),
              accountEmail: Text(userPrimaryEmailAddress),
              currentAccountPicture: CircleAvatar(
                backgroundColor:
                Theme.of(context).platform == TargetPlatform.iOS
                    ? Colors.blue[900]
                    : Colors.white,
                child: Text(
                  firstLetter,
                  style: TextStyle(fontSize: 40.0),
                ),
              ),
            ),

            ListTile(
              title: Text('Notes'),
              onTap: (){
                Navigator.of(context).pop();
                Navigator.push(context, MaterialPageRoute(builder: (context) =>Notes()));
              },
            ),
            ListTile(
              title: Text('Video'),
              onTap: (){
                Navigator.of(context).pop();
                Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
              },
            ),
//            ListTile(
//              title: Text('Subscribe Notes'),
//              onTap: (){
//                Navigator.push(context, MaterialPageRoute(builder: (context) => SubscribeNotes()));
//              },
//            ),
//            ListTile(
//              title: Text('Subscribe Videos'),
//              onTap: (){
//                Navigator.push(context, MaterialPageRoute(builder: (context) => SubscribeVideos()));
//              },
//            ),

            ExpansionTile(
              title: Text("Learning Plans"),

              children: <Widget>[
                Divider(color: Colors.grey.shade400, indent:0.0, height: 1.0),

                ListTile(
                  title: Text('Notes'),
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => SubscribeNotes()));
                  },
                ),
                ListTile(
                  title: Text('Videos'),
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => SubscribeVideos()));
                    // Navigator.pop(context,true);
                    // ignore: unnecessary_statements
                  //  Scaffold.of(context).openEndDrawer;
                    },
                ),

              ],
            ),
            Divider(color: Colors.grey.shade400, indent:0.0, height: 1.0),

            ListTile(
              title: Text('Profile'),
              onTap: (){
                Navigator.of(context).pop();
                Navigator.push(context, MaterialPageRoute(builder: (context) => Profile()));
              },
            ),
            ListTile(
              title: Text('Change Password'),
              onTap: (){
                Navigator.of(context).pop();
                Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePassword()));
              },
            ),
            ListTile(
              title: Text('Logout'),
                onTap: () { _logout(); }//              onTap: (){
//                setState(() {
//                  SharedPreferences preferences = await SharedPreferences.getInstance();
//                  await preferences.remove('KeyNameHere');
//                });
//              },
            ),
          ],
        ),
      ),
        body:Video()
    );
  }
}

// ignore: must_be_immutable
class CustomListTile extends StatelessWidget {
  double maxWidth = 250.0;
  IconData icon;
  String text;
  Function onTap;

  CustomListTile(this.icon, this.text, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child: InkWell(
        splashColor: Colors.blue[900],
        onTap: () {
          Navigator.of(context).pop();
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => Notes()));
        },
        child: Container(
          height: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(icon),
                  Padding(
                    padding: const EdgeInsets.all(7.0),
                    child: Text(
                      text,
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ),
                ],
              ),
              Icon(Icons.arrow_right),

            ],
          ),
        ),
      ),
    );
  }
}
