import 'dart:convert';
//import 'package:filter_list/filter_list.dart';
import 'package:flutter/material.dart';
//import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
//import 'package:dropdown_formfield/dropdown_formfield.dart';
//import 'package:form_field_validator/form_field_validator.dart';
//import 'dashboard.dart';
import 'Menu.dart';
//import 'Login_Screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:path_provider/path_provider.dart';
import 'download_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/cupertino.dart';

class SubscribeNotes extends StatefulWidget {
  @override
  _SubscribeNotesState createState() => _SubscribeNotesState();
}

class _SubscribeNotesState extends State<SubscribeNotes> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text('SUBSCRIBE NOTES'),
              backgroundColor: Colors.blue[900],
              centerTitle: true,
            ),
            body: Center(
                child: SubscribeNotesUser()
            )
        )
    );
  }
}
class SubscribeNotesUser extends StatefulWidget {
  SubscribeNotesUserState createState() => SubscribeNotesUserState();
}

class SubscribeNotesUserState extends State {

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;

  Future<dynamic> futureNotes;
  final _formKey = GlobalKey<FormState>();
  // ignore: unused_field
  TextEditingController _textFieldController = TextEditingController();
  // ignore: unused_field
  String _class;
  // ignore: deprecated_member_use
  List subsnotes=List();
  var accesstoken;
  // ignore: unused_field
  var _accesstokenq;
  var userIdentifier;
  // ignore: unused_field
  var _userIdentifierq;
  var isLoading = false;
  // ignore: deprecated_member_use
  List classes=List();
  String a;
  String _subject;
  String _title;

  //Fetch Class using API
  // ignore: missing_return
  Future<String> fetchAlbum() async {
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Class/GetClasses'),
        headers:{"Accept":"application/json"});
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
    setState((){
      classes=json.decode(res.body);
    });
  }

  Future<dynamic> fetchNotes() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _userIdentifierq = (prefs.getString('UserIdentifier')??'');
    accesstoken = (prefs.getString('accesstoken')??'');

    print(_userIdentifierq);
    Map<String, dynamic> data={'UserIdentifier':int.parse(_userIdentifierq),'GetResources':true,'IsLearningPlan':true};
    var bodyEncoded = json.encode(data);
    print(bodyEncoded);
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Notes/Get'), body:bodyEncoded,
      headers:{"charset":"utf-8","Content-Type": "application/x-www-form-urlencoded","Accept":"application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8","Authorization":accesstoken},
    );
    setState(() {
      isLoading = false;
    });
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));

    setState((){
      subsnotes=json.decode(res.body);
    });

  }


  //  Post Data for Filter
  _postFilterData() async{
    final form = _formKey.currentState;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _userIdentifierq = (prefs.getString('UserIdentifier')??'');
    accesstoken = (prefs.getString('accesstoken')??'');
    print(_userIdentifierq);

    setState(() {
      isLoading = true;
    });
    form.save();
    Map<String, String> headers = {"Content-type": "application/json", "accept" : "application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"};
    var jsons = jsonEncode(<String, dynamic>{'UserIdentifier':_userIdentifierq.toInt(),'GetResources':true,'IsLearningPlan':true,"Name": _subject, "Topic": _title});
    print (jsons);
    var response = await http.post('https://brahmiappservice.azurewebsites.net/Notes/Get', headers: headers, body: jsons);
    setState(() {
      isLoading = false;
    });
    int statusCode = response.statusCode;
    print(statusCode);
    print(response);
    print(response.contentLength);
    print(response.body);
    setState((){
      subsnotes=json.decode(response.body);
    });
  }

  void initState() {
    super.initState();
    futureNotes= fetchNotes();
    print(futureNotes);
    fetchAlbum();
    initializing();
  }

  void initializing() async {
    androidInitializationSettings = AndroidInitializationSettings('app_icon');
    iosInitializationSettings = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = InitializationSettings(
        androidInitializationSettings, iosInitializationSettings);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  void _showProgressNotifications(retStatus) async{
    await progressnotification(retStatus);
  }
  Future<void> progressnotification(retStatus) async {
    AndroidNotificationDetails androidNotificationDetails =
    AndroidNotificationDetails(
        'Channel ID', 'Channel title', 'channel body',
        priority: Priority.High,
        importance: Importance.Max,
        ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
    NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.show(
        0, 'Download Notes', 'Downloading'+retStatus, notificationDetails);
  }


  void _showNotifications() async {
    await notification();
  }


  Future<void> notification() async {
    AndroidNotificationDetails androidNotificationDetails =
    AndroidNotificationDetails(
        'Channel ID', 'Channel title', 'channel body',
        priority: Priority.High,
        importance: Importance.Max,
        ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
    NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.show(
        0, 'Download Video', 'Your Notes  is successfully downloaded', notificationDetails);
  }

  Future<void> notificationAfterSec() async {
    var timeDelayed = DateTime.now().add(Duration(seconds: 5));
    AndroidNotificationDetails androidNotificationDetails =
    AndroidNotificationDetails(
        'second channel ID', 'second Channel title', 'second channel body',
        priority: Priority.High,
        importance: Importance.Max,
        ticker: 'test');

    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();

    NotificationDetails notificationDetails =
    NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.schedule(1, 'Hello there',
        'please subscribe my channel', timeDelayed, notificationDetails);
  }

  // ignore: missing_return
  Future onSelectNotification(String payLoad) {
    if (payLoad != null) {
      print(payLoad);
    }

    // we can set navigator to navigate another screen
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: <Widget>[
        CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              print("");
            },
            child: Text("Okay")),
      ],
    );
  }


  void choiceAction(String choice){
    if(choice == Menu.Subscribe) {}
  }


  _displayDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Filter Notes'),
          content: Container(
              height: 230.0,
              child:SafeArea(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          autocorrect: true,
                          decoration: InputDecoration(
                            labelText: 'Subject',
                          ),
                          onSaved: (String value) {
                            _subject = value;
                          },
                        ),

                        SizedBox(
                          height:10.0,
                        ),


                        DropdownButtonFormField(
                          items: classes.map((item){
                            return new DropdownMenuItem(
                              child:new Text(
                                item['Name'],
                                style: TextStyle(
                                    fontSize: 15.0
                                ),
                              ),
                              value:item['Identifier'].toString(),
                            );
                          }).toList(),
                          decoration: InputDecoration(
                            labelText: 'Class',
                          ),
                          onChanged: (String newValue) {
                            setState(() {
                              a=newValue;
                              print(a.toString()) ;
                            });
                          },
                          onSaved: (String item) {
                            _class = item;
                          },
                        ),

                        SizedBox(
                          height:25.0,
                        ),

                        TextFormField(
                          autocorrect: true,
                          decoration: InputDecoration(
                            labelText: 'Notes Title',
                          ),
                          onSaved: (String value) {
                            _title = value;
                          },
                        ),

                      ],
                    ),
                  )
              )
          ),
          actions: <Widget>[
            // ignore: deprecated_member_use
            FlatButton( color: Colors.deepPurple,
                child: const Text('APPLY'),
                onPressed:(){
                  _postFilterData();
                  Navigator.of(context).pop();
                }
            ),
            // ignore: deprecated_member_use
            FlatButton(
              color: Colors.deepPurple,
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

//  static const String routeName = '/notesPage';
  @override
  Widget build(BuildContext context)  {
    var fileDownloaderProvider =
    Provider.of<FileDownloaderProvider>(context, listen: false);
    return Scaffold(
        backgroundColor: Colors.grey[300],
        body:  isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            :
        Container(

            child:Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Row(
                    children: <Widget>[
                      // ignore: deprecated_member_use
                      RaisedButton(
                        color: Colors.deepPurple,
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.filter_list,color: Colors.white),
                            Text('FILTER',
                              style: TextStyle(
                                  color: Colors.white
                              ),),
                          ],
                        ),
                        onPressed: () {
                          _displayDialog(context);
                        },
                      ),


                    ],
                  ),
                ),

                Expanded(
                    child:Container(
                      child:ListView.builder(
                          itemCount: subsnotes.length,
                          itemBuilder: (BuildContext context,int index){
                            return Card(
                              child: Container(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Image.network(
                                              '${subsnotes[index]['Resource']['ThumbnailURL']}',
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
//
                                    Container(
                                      color:Colors.white,
                                      padding: const EdgeInsets.all(15),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            /*1*/
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                /*2*/
                                                Container(
                                                  child: Text(
                                                    'Topic: ${subsnotes[index]['Topic']}',
                                                    style: TextStyle(
                                                      fontSize: 18.0,
                                                      fontWeight: FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                                Text(
                                                  '(${subsnotes[index]['Name']})',
                                                  style: TextStyle(
                                                    color: Colors.grey[500],
                                                    fontSize: 16.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                Text(
                                                  'Cost: ${subsnotes[index]['Cost']}',
                                                  style: TextStyle(
                                                    color: Colors.red[900],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          /*3*/
                                        dowloadButton(fileDownloaderProvider,'${subsnotes[index]['Resource']['URL']}','${subsnotes[index]['Resource']['Name']}'),
                                        downloadProgress(),
//                                          PopupMenuButton<String>(
//                                            onSelected: choiceAction,
//                                            itemBuilder: (BuildContext context){
//                                              return Menu.choices.map((String choice){
//                                                return PopupMenuItem<String>(
//                                                  value: choice,
//                                                  child: Text(choice),
//                                                );
//                                              }).toList();
//                                            },
//                                          ),
                                        ],
                                      ),
                                    ),


                                    Divider(color: Colors.grey.shade400, indent:0.0, height: 1.0),



                                  ],
                                ),

                              ),
                            );
                          }
                      ),
                    )

                )
              ],
            )

        )
    );
  }

  Widget dowloadButton(FileDownloaderProvider downloaderProvider, url, name) {
    // ignore: deprecated_member_use
    return new FlatButton(
      onPressed: () {
        downloaderProvider
            .downloadFile(url, name+'.exo')
            .then((onValue) {});
      },
      textColor: Colors.black,
      child: new Icon(Icons.file_download,color: Colors.black),
    );
  }



  Widget downloadProgress() {
    var fileDownloaderProvider =
    Provider.of<FileDownloaderProvider>(context, listen: true);

    return new Text(
      downloadStatus(fileDownloaderProvider),
      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
    );
  }

  downloadStatus(FileDownloaderProvider fileDownloaderProvider) {
    var retStatus = "";
    var status = "";
    switch (fileDownloaderProvider.downloadStatus) {
      case DownloadStatus.Downloading:
        {
          status = "Download Progress : " +
              fileDownloaderProvider.downloadPercentage.toString() +
              "%";
          _showProgressNotifications(status);
        }
        break;
      case DownloadStatus.Completed:
        {
          retStatus = "";
          _showNotifications();
        }
        break;
      case DownloadStatus.NotStarted:
        {
          retStatus = "";
        }
        break;
      case DownloadStatus.Started:
        {
          retStatus = "";
        }
        break;
    }

    return retStatus;
  }

}