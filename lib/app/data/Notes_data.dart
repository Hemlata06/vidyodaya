// ignore: camel_case_types
class Notes_data {
  // ignore: non_constant_identifier_names
  final int Identifier;
  // ignore: non_constant_identifier_names
  final String Name;
  // ignore: non_constant_identifier_names
  final int ResourceIdentifier;
  // ignore: non_constant_identifier_names
  final String Topic;
  // ignore: non_constant_identifier_names
  final int ClassIdentifier;
  // ignore: non_constant_identifier_names
  final double Cost;
  // ignore: non_constant_identifier_names
  final String Subject;
  // ignore: non_constant_identifier_names
  final int Class;
  Resource resource;

  Notes_data(this.Identifier, this.Name, this.ResourceIdentifier, this.Topic, this.ClassIdentifier, this.Cost, this.Subject,this.Class, this.resource);
  factory Notes_data.fromJson(Map<String, dynamic> json) {
    return Notes_data(
      json['Identifier'],
      json['Name'],
      json['ResourceIdentifier'],
      json['Topic'],
      json['ClassIdentifier'],
      json['Cost'],
      json['Subject'],
      json['Class'],
        json['resource']
    );
  }
}
//class Mac{
//  MacUser user;
//
//  Mac({
//    this.user
//  });
//}

class Resource{
  int identifier;
  String uRL;
  String name;
  String type;
  String thumbnailURL;
  String description;
  String fileType;
  String fileSize;
  String resourceFile;

  Resource({
    this.identifier,
    this.uRL,
    this.name,
    this.type,
    this.thumbnailURL,
    this.description,
    this.fileType,
    this.fileSize,
    this.resourceFile
  });
}

