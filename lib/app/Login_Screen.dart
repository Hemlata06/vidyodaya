import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'register.dart';
import 'Forgot_Password.dart';
import 'dashboard.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'download_provider.dart';
//import 'home.dart';
//import 'video.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _State();
}

class _State extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => FileDownloaderProvider(),
            child: LoginUser(),
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
            home: Scaffold(
                appBar: AppBar(
                  title: Text('Vidyodaya'),
                  backgroundColor: Colors.blue[900],
                  centerTitle: true,
                ),
                body: Center(
                    child: LoginUser()
                )
            )
        )
    );
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//        home: Scaffold(
//            appBar: AppBar(
//              title: Text('BRAHMI'),
//              backgroundColor: Colors.blue[900],
//              centerTitle: true,
//            ),
//            body: Center(
//                child: LoginUser()
//            )
//        )
//    );
//  }
  }
}
class LoginUser extends StatefulWidget {
  LoginUserState createState() => LoginUserState();
}

//class Dialogs {
//  static Future<void> showLoadingDialog(
//      BuildContext context, GlobalKey key) async {
//    return showDialog<void>(
//        context: context,
//        barrierDismissible: false,
//        builder: (BuildContext context) {
//          return new WillPopScope(
//              onWillPop: () async => false,
//              child: SimpleDialog(
//                  key: key,
//                  backgroundColor: Colors.black54,
//                  children: <Widget>[
//                    Center(
//                      child: Column(children: [
//                        CircularProgressIndicator(),
//                        SizedBox(height: 10,),
//                        Text("Please Wait....",style: TextStyle(color: Colors.blueAccent),)
//                      ]),
//                    )
//                  ]));
//        });
//  }
//}

class LoginUserState extends State {
  // ignore: unused_field
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  final _formKey = GlobalKey<FormState>();
  final requiredValidator = RequiredValidator(errorText: 'this field is required');

  // For CircularProgressIndicator.
  String _username;
  String _password;
  bool _autoValidate=false;
  // ignore: unused_field
  bool _visible=false;
  var accesstoken;
  var userIdentifier;
  // ignore: non_constant_identifier_names
  var error_description;
  var userPrimaryEmailAddress;
  var userName;

  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  var isLoading = false;

  void _validateInputs() async{
    setState(() {
      isLoading = true;
    });

//    Dialogs.showLoadingDialog(context, _keyLoader);//invoking login
//    Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();//close the dialoge

    final form = _formKey.currentState;
    if (form.validate()) {

      _visible=true;
      // Text forms was validated.
      form.save();
      print('Success Login');
      print(_username);
      print(_password);
      Map<String, dynamic> bodys = {"grant_type": "password","client_id": "c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8",'username': _username, 'password': _password};
      String encodedBody = bodys.keys.map((key) => "$key=${bodys[key]}").join("&");
      var bodyEncoded = json.encode(bodys);
      print(bodyEncoded);
      var response = await http.post('https://brahmiappservice.azurewebsites.net/Login', body: encodedBody , headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },);
      setState(() {
        isLoading = false;
      });
      int statusCode = response.statusCode;

      print(statusCode);
      print(response);
      print(response.contentLength);
      print(response.body);
      // ignore: unused_local_variable
      String body = response.body;
      Map mapRes = json.decode(response.body);
//      _showMyDialog();
      error_description = mapRes['error_description'];
      accesstoken = mapRes['access_token'];
      userIdentifier = mapRes['UserIdentifier'];
      userPrimaryEmailAddress = mapRes['UserPrimaryEmailAddress'];
      userName = mapRes['UserName'];

      if(accesstoken != null)
      {
        print('user valid');
        var preferences = await SharedPreferences.getInstance();// Save a value
        preferences.setString('accesstoken', accesstoken);// Retrieve value later
        // ignore: unused_local_variable
        var savedValue = preferences.getString('accesstoken');
        preferences.setString('UserIdentifier', userIdentifier);// Retrieve value later
        // ignore: unused_local_variable
        var saveUserIdentifier = preferences.getString('UserIdentifier');
        preferences.setString('UserPrimaryEmailAddress', userPrimaryEmailAddress);// Retrieve value later
        var savedValue1 = preferences.getString('UserPrimaryEmailAddress');
        print(savedValue1);
        preferences.setString('UserName', userName);// Retrieve value later
        var savedValue2 = preferences.getString('UserName');
        print(savedValue2);

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Dashboard()));
      }
      else{
        _visible=true;
        print('user invalid');
        _showMyDialog();
        Fluttertoast.showToast(
            msg: "Invalid Credentials",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red[300],
            textColor: Colors.white,
            fontSize: 16.0
        );
      }

    } else {
      print('Not true Validate');
      setState(() {
        isLoading = false;
      });
      setState(() => _autoValidate = true);
    }

  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert Message'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(error_description),
              ],
            ),
          ),
          actions: <Widget>[
            // ignore: deprecated_member_use
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:SingleChildScrollView(
            child: Center(
              child: Column(
                children: <Widget>[
//                  Divider(),
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          children: [
                            Image(
                              image:AssetImage('assets/logooo.jpg'),
                              fit: BoxFit.contain,
                              width: 180,
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              'Login!',
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 30),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),

                  Container(
                    child:Form(
                      key: _formKey,
                        // ignore: deprecated_member_use
                        autovalidate: _autoValidate,
                      child:Container(
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                    child: Container(
                                      padding: EdgeInsets.all(10.0),
                                      child: TextFormField(
                                        controller: emailController,
                                        autocorrect: true,
                                        decoration: InputDecoration( border: OutlineInputBorder(),
                                          labelText: 'User Name',
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                            borderSide: BorderSide(color: Colors.grey, width: 2),
                                          ),),
                                        validator: (value) {
                                          if(value.isEmpty)
                                            return 'Username is Required';
                                          else if (value.length < 3)
                                            return 'Username must be of Email';
                                          else
                                            return null;
                                        },
                                        onSaved: (String value) {
                                          _username = value;
                                          print("On save ");
                                        },
                                      ),
                                    )
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                    child:Container(
                                      padding: EdgeInsets.all(10.0),
                                      child:  TextFormField(
                                        controller: passwordController,
                                        autocorrect: true,
                                        obscureText: true,
                                        decoration: InputDecoration( border: OutlineInputBorder(),
                                          labelText: 'Password',
//                                          validator: requiredValidator,
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                            borderSide: BorderSide(color: Colors.grey, width: 2),
                                          ),),
                                        validator: (value) {
                                          if(value.isEmpty)
                                            return 'Password is Required';
                                          else if (value.length < 5)
                                            return 'Password must be more than 5 character';
                                          else
                                            return null;
                                        },
                                        onSaved: (String val) {
                                          _password = val;
                                        },
                                      ),
                                    )
                                ),

                              ],
                            ),


                            Container(
                                child: Row(
                                  children: <Widget>[
//                                    Text('Does not have account?'),
                                    // ignore: deprecated_member_use
                                    FlatButton(
                                      textColor: Colors.blue,
                                      child: Text(
                                        'Forgot Password?',
                                        style: TextStyle(fontSize: 17),
                                      ),
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => ForgotPassword(),
                                          ),
                                        );
                                      },
                                    )
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.center,
                                )),


//                            Row(
//                              children: <Widget>[
//                                RaisedButton(
//                                  shape: StadiumBorder(),
//                                  textColor: Colors.white,
//                                  color: Colors.pink[900],
//                                  child: Text('VIDEO'),
//                                  onPressed: () {
//                                    Navigator.push(
//                                      context,
//                                      MaterialPageRoute(
//                                        builder: (context) => HomePage(),
//                                      ),
//                                    );
//                                  },
//                                )
//                              ],
//                            ),
//
//                            Row(
//                              children: <Widget>[
//                                RaisedButton(
//                                  shape: StadiumBorder(),
//                                  textColor: Colors.white,
//                                  color: Colors.teal[900],
//                                  child: Text('VIDEO SECTION'),
//                                  onPressed: () {
//                                    Navigator.push(
//                                      context,
//                                      MaterialPageRoute(
//                                        builder: (context) => Video(),
//                                      ),
//                                    );
//                                  },
//                                )
//                              ],
//                            ),
//
//
//                            Row(
//                              children: <Widget>[
//                                RaisedButton(
//                                  shape: StadiumBorder(),
//                                  textColor: Colors.white,
//                                  color: Colors.teal[900],
//                                  child: Text('DASHBOARD'),
//                                  onPressed: () {
//                                    Navigator.push(
//                                      context,
//                                      MaterialPageRoute(
//                                        builder: (context) => Dashboard(),
//                                      ),
//                                    );
//                                  },
//                                )
//                              ],
//                            ),


                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                isLoading
                                    ? Center(
                                  child: CircularProgressIndicator(),
                                ):
                                Container(
                                    margin: EdgeInsets.only(top: 20),
                                    height: 50,
                                    width:200,
                                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    // ignore: deprecated_member_use
                                    child: RaisedButton(
                                      shape: StadiumBorder(),
                                      textColor: Colors.white,
                                      color: Colors.blue[900],
                                      child: Text('Login'),
                                      onPressed:(_validateInputs),

                                    )),

                              ],
                            )
                          ],
                        ),
                      )
                  )),

                  Container(
                      child: Row(
                        children: <Widget>[
                          Text('Does not have account?'),
                          // ignore: deprecated_member_use
                          FlatButton(
                            textColor: Colors.blue,
                            child: Text(
                              'Register',
                              style: TextStyle(fontSize: 20),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Register(),
                                ),
                              );
                            },
                          )
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      )),

//                  Visibility(
//                      visible: _visible,
//                      maintainSize: true,
//                      maintainAnimation: true,
//                      maintainState: true,
//                      child: Container(
//                          margin: EdgeInsets.only(bottom: 30),
//                          child: CircularProgressIndicator()
//                      )
//                  ),

                ],
              ),
            )));
  }
}

