import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:form_field_validator/form_field_validator.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text('Forgot Password'),
              backgroundColor: Colors.blue[900],
              centerTitle: true,
            ),
            body: Center(
                child: ForgotPasswordUser()
            )
        )
    );
  }
}
class ForgotPasswordUser extends StatefulWidget {
  ForgotPasswordUserState createState() => ForgotPasswordUserState();
}

class ForgotPasswordUserState extends State {

  // ignore: unused_field
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _formKey = GlobalKey<FormState>();
  final requiredValidator = RequiredValidator(errorText: 'this field is required');

  var isLoading = false;
  String _email;
  bool _autoValidate=false;
  // ignore: non_constant_identifier_names
  var error_description;

  void _postdata() async{
    setState(() {
      isLoading = true;
    });

    final form = _formKey.currentState;
    if (form.validate()) {
      // Text forms was validated.
      form.save();
      print('Success Login');
      print(_email);
      var email=_email;

      var response = await http.post(Uri.encodeFull(
          'https://brahmiappservice.azurewebsites.net/User/ResetPassword?userEmailAddress=$email'),
          headers: {
            "charset": "utf-8",
            "Content-Type": "application/x-www-form-urlencoded"
          }
      );
      print(response);
      setState(() {
        isLoading = false;
      });
      int statusCode = response.statusCode;
      print(statusCode);
      print(response.contentLength);
      print(response.body);
      Map mapRes = json.decode(response.body);

      error_description = mapRes['Error']['Message'];
      if(error_description!=null){
        _showMyDialog();
      }
      else{
        Fluttertoast.showToast(
            msg: "Please check your Mail",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }

    } else {
      print('Not true Validate');
      setState(() {
        isLoading = false;
      });
      setState(() => _autoValidate = true);
    }

  }


  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert Message'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(error_description),
              ],
            ),
          ),
          actions: <Widget>[
            // ignore: deprecated_member_use
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:
        isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            :SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 30),
                  padding: EdgeInsets.all(20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                            Text(
                              'Forgot Password!',
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w500,
                                  fontSize:25),
                            ),
                            Image(
                              image:AssetImage('assets/lock.png'),
                              fit: BoxFit.contain,
                              width: 180,
                            ),
                           Container(
                             child: Text(
                               'Enter the email address associated with your account.',
                               textAlign: TextAlign.center,
                               style: TextStyle(
                                   color: Colors.blueGrey,
                                   fontWeight: FontWeight.w500,
                                   fontSize:18),
                             )
                           ),
                            Container(
                                child: Text(
                                  'We will send you link to reset your password',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize:15),
                                )
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                    child: Form(
                                      key: _formKey,
                                      // ignore: deprecated_member_use
                                      autovalidate: _autoValidate,
                                      child: Container(
                                        margin: EdgeInsets.only(top: 20),
                                        padding: EdgeInsets.all(10.0),
                                        child: TextFormField(
                                          autocorrect: true,
                                          decoration: InputDecoration( border: OutlineInputBorder(),
                                            labelText: 'Email Address',
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                              borderSide: BorderSide(color: Colors.grey, width: 2),
                                            ),),
                                          validator: (value) {
                                            if(value.isEmpty)
                                              return 'Email is Required';
                                            else if (value.length < 3)
                                              return 'Invalid Email';
                                            else
                                              return null;
                                          },
                                          onSaved: (String value) {
                                            _email = value;
                                            print("On save ");
                                          },
                                        ),
                                      ),
                                    )
                                ),
                              ],
                            ),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                isLoading
                                    ? Center(
                                  child: CircularProgressIndicator(),
                                ):
                                Container(
                                    margin: EdgeInsets.only(top: 20),
                                    height: 50,
                                    width:200,
                                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    // ignore: deprecated_member_use
                                    child: RaisedButton(
                                      shape: StadiumBorder(),
                                      textColor: Colors.white,
                                      color: Colors.blue[900],
                                      child: Text('Send'),
                                      onPressed:(_postdata),

                                    )),

                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          )
        )
    );
  }
}