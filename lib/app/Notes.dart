import 'dart:convert';
//import 'package:filter_list/filter_list.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
//import 'dashboard.dart';
import 'Menu.dart';
//import 'package:progress_dialog/progress_dialog.dart';
import 'Subscribe.dart';
//import 'SubscribeVideoAndNotes.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:form_field_validator/form_field_validator.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'data/Notes_data.dart';

class Notes extends StatefulWidget {
  @override
  _NotesState createState() => _NotesState();
}

class _NotesState extends State<Notes> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text('NOTES'),
              backgroundColor: Colors.blue[900],
              centerTitle: true,
            ),
            body: Center(
                child: NotesUser()
            )
        )
    );
  }
}
class NotesUser extends StatefulWidget {
  NotesUserState createState() => NotesUserState();
}

class NotesUserState extends State {

//  Declaring Variables
  Future<dynamic> futureNotes;
  Razorpay _razorpay;
  final _formKey = GlobalKey<FormState>();
  // ignore: unused_field
  String _class;
  // ignore: deprecated_member_use
  List notes=List();
  var selectednotes;
  var isLoading = false;
  int pageNumber;
  // ignore: deprecated_member_use
  List classes=List();
  // ignore: deprecated_member_use
  List list=List();
  String a;
  var accesstoken;
  var _accesstokenq;
  String _subject;
  String _title;
  // ignore: unused_field
  String _selectedclass;
  String title;
  var orderid;
  var userIdentifier;
  // ignore: unused_field
  var _userIdentifierq;
  double amt=0.0;
  int userId=0;
  int totalamount;
   List<Notes_data> products;


  //Fetch Class using API
  // ignore: missing_return
  Future<String> fetchAlbum() async {
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Class/GetClasses'),
        headers:{"Accept":"application/json"});
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
    setState((){
      classes=json.decode(res.body);
    });
  }

  //Fetch Notes using API
  // ignore: missing_return
  Future<Notes_data> fetchNotes() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _accesstokenq = (prefs.getString('accesstoken')??'');

    setState(() {
      isLoading = true;
    });
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Notes/Get'), body:{'Identifier':''},
        headers:{"charset":"utf-8","Content-Type": "application/x-www-form-urlencoded","Accept":"application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8","Authorization":_accesstokenq},
    );

    setState(() {
      isLoading = false;
    });
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
    setState((){
      notes=json.decode(res.body);
    });
  }

  //  Post Data for Filter
  _postFilterData() async{
    final form = _formKey.currentState;
      setState(() {
        isLoading = true;
      });
      form.save();
      Map<String, String> headers = {"Content-type": "application/json", "accept" : "application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"};
      var jsons = jsonEncode(<String, String>{"Name": _subject, "Topic": _title});
      print (jsons);
      var response = await http.post('https://brahmiappservice.azurewebsites.net/Notes/Get', headers: headers, body: jsons);
      setState(() {
        isLoading = false;
      });
      int statusCode = response.statusCode;
      print(statusCode);
      print(response);
      print(response.contentLength);
      print(response.body);
      setState((){
        notes=json.decode(response.body);
      });
  }


  @override
  void initState() {
    super.initState();
    futureNotes= fetchNotes();
    print(futureNotes);
    fetchAlbum();
    _razorpay = new Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay = new Razorpay();
    _razorpay.clear(); // Removes all listeners
  }

  var price;
  // ignore: missing_return
  Future<Notes_data> choiceAction(choice) async{
    print(choice);
    print(choice.runtimeType);
    print(jsonEncode(choice).runtimeType);

    if(choice != null){
      setState(() {
        isLoading = true;
      });

      Map<String, String> headers = {"Content-type": "application/json", "accept" : "application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"};
      var jsons = jsonEncode(<String, dynamic>{"Identifiers": 6});
      print (jsons);
      var response = await http.post('https://brahmiappservice.azurewebsites.net/Notes/Get', headers: headers, body: jsons);
      print('..........');
      print(response.body);
      print(json.decode(response.body)[0]);
      selectednotes=response.body;
      print(selectednotes);
      setState(() {
        amt=json.decode(response.body)[0]['Cost'];
        price=json.decode(response.body)[0]['Cost'];
        print(amt);
        selectednotes=response.body;
      });
      print('..........');
      print('..........');
      openCheckout(amt);

      setState(() {
        isLoading = false;
      });
    }

    // ignore: unused_local_variable
    var test= {
      "UserIdentifier": _userIdentifierq,
      "Notes": selectednotes
    };
  }

  Future<dynamic> openCheckout(amt) async {
    setState(() {
      isLoading = true;
    });
    print(amt);
    int a = amt.toInt();
    setState(() {
      totalamount=a*100;
    });
    print(a);
    var amts =a * 100;
    var res = await http.post(Uri.encodeFull(
        'https://brahmiappservice.azurewebsites.net/Payment/CreateOrder?amount=$amts'),
        headers: {
          "charset": "utf-8",
          "Content-Type": "application/x-www-form-urlencoded"
        }
    );

    print('..........');
    print(res);
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body)['Attributes']['id']);
    orderid = json.decode(res.body)['Attributes']['id'];
    print('..........');
    print('..........');

    setState(() {
      isLoading = false;
    });
    var options = {
      'key': 'rzp_live_tF2U2M1e2FXGkN',
      'amount':totalamount, //in the smallest currency sub-unit.
      'name': 'Brahmi',
      'order_id': orderid, // Generate order_id using Orders API
      'description': 'Notes',
      'timeout': 60, // in seconds
      'prefill': {
        'contact': '+919140310165',
        'email': 'riteshseth1990@gmail.com'
      }
    };

    try {
      _razorpay.open(options);
    }
    catch (e) {
      print(e.toString());
    }

    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);

    setState(() {
      a = amt.toInt();
    });
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
//    _UserIdentifierq = (prefs.getString('UserIdentifier') ?? '');
    _userIdentifierq = (prefs.getString('UserIdentifier') ?? '');
    print('Payment Success');
    Fluttertoast.showToast(
        msg: "Payment Successful",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0
    );
    print('..........');
    print(response.paymentId);
    print(response.orderId);
    print(response.signature);
    print('..........');
    // ignore: unused_local_variable
    var x=price*100;
    int number = int.parse(_userIdentifierq);
    var dataBody = {};
    dataBody["OrderId"] = response.orderId;
    dataBody["PaymentId"] =response.paymentId;
    dataBody["Signature"] =response.signature;
    dataBody["Amount"] =totalamount.toString();
    dataBody["UserIdentifier"] =number;
    var orderdata = {};
    orderdata= dataBody;
    String order = json.encode(orderdata);
    print(order);

//    var data = {
//      "OrderId": response.orderId,
//      "PaymentId": response.paymentId,
//      "Signature": response.signature,
//      "Amount": price.toString(),
//      "UserIdentifier": _UserIdentifierq
//    };
//    print('----------');
//    print(data);
//    print('----------');
    var res = await http.post(Uri.encodeFull(
        'https://brahmiappservice.azurewebsites.net/Payment/CapturePayment'),
        body: order,
        headers: {
          "charset": "utf-8",
          "Content-Type": "application/x-www-form-urlencoded",
          "TenantCode": "c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"
        }
    );
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
    print('######################');

    var resBody = {};
    resBody["UserIdentifier"] = _userIdentifierq;
    resBody["Notes"] = jsonDecode(selectednotes);
    var user = {};
    user= resBody;
    String bodydata = json.encode(user);
    print(bodydata);

    var resp = await http.post(Uri.encodeFull(
        'https://brahmiappservice.azurewebsites.net/Subscription/SubscribeVideoOrNotes'),
        body:bodydata,
        headers: {
          "charset": "utf-8",
          "Content-Type": "application/x-www-form-urlencoded",
          "TenantCode": "c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"
        }
    );
    print(resp.body);
    print(resp.statusCode);

  }

  void _handlePaymentError(PaymentFailureResponse response) {
    print('Payment Error');
    Fluttertoast.showToast(
        msg: "Payment Error",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red[300],
        textColor: Colors.white,
        fontSize: 16.0
    );

  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    print('External Wallet');
    Fluttertoast.showToast(
        msg: "External Wallet",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red[300],
        textColor: Colors.white,
        fontSize: 16.0
    );
  }



  _displayDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Filter Notes'),
          content: Container(
              height: 230.0,
            child:SafeArea(
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      autocorrect: true,
                      decoration: InputDecoration(
                        labelText: 'Subject',
                      ),
                      onSaved: (String value) {
                        _subject = value;
                      },
                    ),

                    SizedBox(
                      height:10.0,
                    ),


                    DropdownButtonFormField(
                      items: classes.map((item){
                        return new DropdownMenuItem(
                          child:new Text(
                            item['Name'],
                            style: TextStyle(
                                fontSize: 15.0
                            ),
                          ),
                          value:item['Identifier'].toString(),
                        );
                      }).toList(),
                      decoration: InputDecoration(
                        labelText: 'Class',
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          a=newValue;
                          print(a.toString()) ;
                        });
                      },
                      onSaved: (String item) {
                        _class = item;
                      },
                    ),

                    SizedBox(
                      height:25.0,
                    ),

                    TextFormField(
                      autocorrect: true,
                      decoration: InputDecoration(
                        labelText: 'Notes Title',
                      ),
                      onSaved: (String value) {
                        _title = value;
                      },
                    ),

                  ],
                ),
              )
            )
          ),
          actions: <Widget>[
            // ignore: deprecated_member_use
            FlatButton( color: Colors.deepPurple,
              child: const Text('APPLY'),
              onPressed:(){
                _postFilterData();
                Navigator.of(context).pop();
              }
            ),
            // ignore: deprecated_member_use
            FlatButton(
              color: Colors.deepPurple,
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }



  @override


   Widget build(BuildContext context)  {

    return Scaffold(
      backgroundColor: Colors.grey[300],
      body:  isLoading
        ? Center(
        child: CircularProgressIndicator(),
    )
        :
    Container(

        child:Column(
          children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(3.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                  children: <Widget>[
                      // ignore: deprecated_member_use
                      RaisedButton(
                        color: Colors.deepPurple,
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.filter_list,color: Colors.white),
                            Text('FILTER',
                              style: TextStyle(
                                  color: Colors.white
                              ),),
                          ],
                        ),
                        onPressed: () {
                          _displayDialog(context);
                        },
                      ),

                      SizedBox(
                        width:10.0 ,
                      ),
                      // ignore: deprecated_member_use
                      RaisedButton(
                        color: Colors.deepPurple,
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.add,color: Colors.white),
                            Text('SUBSCRIBE',
                              style: TextStyle(
                                  color: Colors.white
                              ),),
                          ],
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Subscribe(),
                            ),
                          );
                        },
                      ),
                    ],
                ),
              ),


              Expanded(
                  child:Container(
                    child:ListView.builder(
                        itemCount: notes.length,
                        itemBuilder: (BuildContext context,int index){
                          return Card(
                            child: Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(0.0),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Image.network(
                                            '${notes[index]['Resource']['ThumbnailURL']}',
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
//
                                  Container(
                                    color:Colors.white,
                                    padding: const EdgeInsets.all(15),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          /*1*/
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              /*2*/
                                              Container(
                                                child: Text(
                                                  'Topic: ${notes[index]['Topic']}',
                                                  style: TextStyle(
                                                    fontSize: 18.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                '(${notes[index]['Name']})',
                                                style: TextStyle(
                                                  color: Colors.grey[500],
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              Text(
                                                'Cost: ${notes[index]['Cost']}',
                                                style: TextStyle(
                                                  color: Colors.red[900],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        /*3*/
//                                        dowloadButton(fileDownloaderProvider,'${videos[index]['Resource']['ThumbnailURL']}'),
//                                        downloadProgress(),
                                        PopupMenuButton<String>(
                                          onSelected: choiceAction,
                                          itemBuilder: (BuildContext context){
                                            return Menu.choices.map((String choice){
                                              return PopupMenuItem<String>(
                                                value:'${notes[index]}',
                                                child: Text(choice),
                                              );
                                            }).toList();
                                          },
                                        ),
                                      ],
                                    ),
                                  ),


                                  Divider(color: Colors.grey.shade400, indent:0.0, height: 1.0),



                                ],
                              ),

                            ),
                          );
                        }
                    ),
                  )

              )
          ],
        )

      )
    );
  }



}