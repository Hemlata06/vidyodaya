import 'dart:convert';
// ignore: unused_import
import 'package:filter_list/filter_list.dart';
// ignore: unused_import
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
//import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
// ignore: unused_import
import 'package:dropdown_formfield/dropdown_formfield.dart';
// ignore: unused_import
import 'package:form_field_validator/form_field_validator.dart';
// ignore: unused_import
import 'dashboard.dart';
import 'Menu.dart';
// ignore: unused_import
import 'chewie_list_item.dart';
// ignore: unused_import
import 'package:video_player/video_player.dart';
// ignore: unused_import
import 'Login_Screen.dart';
import 'package:path_provider/path_provider.dart';
// ignore: unused_import
import 'download_provider.dart';
// ignore: unused_import
import 'package:provider/provider.dart';
import 'dart:io';
// ignore: unused_import
import 'package:paging/paging.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'SubscribeVideo.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'data/Notes_data.dart';
//import 'package:flick_video_player/flick_video_player.dart';

class Video extends StatefulWidget {
  @override
  _VideoState createState() => _VideoState();
}

class _VideoState extends State<Video> {
  @override

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: Scaffold(
        
            body: Center(
                child: VideoUser()
            )
        )
    );
  }
}

class VideoUser extends StatefulWidget {
  VideoUserState createState() => VideoUserState();
}

class VideoUserState extends State {
  final _formKey = GlobalKey<FormState>();

//  DOWNLOAD FUNCTIONALITY
  _createFolder()async {
    final directory = await getExternalStorageDirectory();
    final folderName = "brahmi";
    final path = Directory("${directory.path}/$folderName");
    if ((await path.exists())) {
     
      // print("exist");
    } else {
     
      // print("not exist");
      path.create();
    }
  }
  Future<dynamic> futureNotes;

  // ignore: unused_field
  String _class;
  // ignore: deprecated_member_use
  List notes=List();
  // ignore: deprecated_member_use
  List videos=List();
  // ignore: non_constant_identifier_names
  var Name;
  Razorpay _razorpay;
  var isLoading = false;
  // ignore: deprecated_member_use
  List classes=List();
  String a;
  var accesstoken;
  var _accesstokenq;
  String _subject;
  String _name;
  // ignore: unused_field
  String _selectedclass;
  var orderid;
  // ignore: non_constant_identifier_names
  var UserIdentifier;
  // ignore: non_constant_identifier_names
  var _UserIdentifierq;
  double amt=0.0;
  // ignore: non_constant_identifier_names
  int UserId=0;
  int totalamount;
  var selectedvideos;
  var _topic;
  List<Notes_data> products;

  // ignore: missing_return
  Future<String> fetchAlbum() async {
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Class/GetClasses'),
        headers:{"Accept":"application/json"});

    setState((){
      classes=json.decode(res.body);
    });
  }

  Future<dynamic> fetchVideo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _accesstokenq = (prefs.getString('accesstoken')??'');

    setState(() {
      isLoading = true;
    });
    var res=await http.post(Uri.encodeFull('https://brahmiappservice.azurewebsites.net/Video/Get'), body:{'Identifier':''},
        headers:{"charset":"utf-8","Content-Type": "application/x-www-form-urlencoded","Accept":"application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8","Authorization":_accesstokenq}
    );
    setState(() {
      isLoading = false;
    });

    setState((){
      videos=json.decode(res.body);
    });
  }

  void initState(){
    WidgetsFlutterBinding.ensureInitialized();
    super.initState();
    _createFolder();
    futureNotes= fetchVideo();
    // print(futureNotes);
    fetchAlbum();
    _razorpay = new Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay = new Razorpay();
    _razorpay.clear(); // Removes all listeners
  }

  var price;
  // ignore: missing_return
  Future<Notes_data> choiceAction(choice) async{
    // print(choice);
    // print(choice.runtimeType);
    // print(jsonEncode(choice).runtimeType);

    if(choice != null){
      setState(() {
        isLoading = true;
      });

      Map<String, String> headers = {"Content-type": "application/json", "accept" : "application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"};
      var jsons = jsonEncode(<String, dynamic>{"Identifiers":int.parse(choice)});
      var response = await http.post('https://brahmiappservice.azurewebsites.net/Video/Get', headers: headers, body: jsons);
      selectedvideos=response.body;
      setState(() {
        amt=json.decode(response.body)[0]['Cost'];
        price=json.decode(response.body)[0]['Cost'];
        // print(amt);
        selectedvideos=response.body;
      });
      openCheckout(amt);
      setState(() {
        isLoading = false;
      });
    }
  }

  Future<dynamic> openCheckout(amt) async {
    setState(() {
      isLoading = true;
    });
    // print(amt);
    int a = amt.toInt();
    setState(() {
      totalamount=a*100;
    });
    // print(a);
    var amts =a * 100;
    var res = await http.post(Uri.encodeFull(
        'https://brahmiappservice.azurewebsites.net/Payment/CreateOrder?amount=$amts'),
        headers: {
          "charset": "utf-8",
          "Content-Type": "application/x-www-form-urlencoded"
        }
    );

    // print(res);
    // print(res.statusCode);
    // print(res.body);
    // print(json.decode(res.body)['Attributes']['id']);
    orderid = json.decode(res.body)['Attributes']['id'];


    var options = {
      'key': 'rzp_live_tF2U2M1e2FXGkN',
      'amount':totalamount, //in the smallest currency sub-unit.
      'name': 'Brahmi',
      'order_id': orderid, // Generate order_id using Orders API
      'description': 'Notes',
      'timeout': 60, // in seconds
      'prefill': {
        'contact': '+919140310165',
        'email': 'riteshseth1990@gmail.com'
      }
    };

    try {
      _razorpay.open(options);
    }
    catch (e) {
      // print(e.toString());
    }

    setState(() {
      a = amt.toInt();
    });
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _UserIdentifierq = (prefs.getString('UserIdentifier') ?? '');
    Fluttertoast.showToast(
        msg: "Payment Successful",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0
    );
    // print(response.paymentId);
    // print(response.orderId);
    // print(response.signature);
    // var x=price*100;
    int number = int.parse(_UserIdentifierq);
    var dataBody = {};
    dataBody["OrderId"] = response.orderId;
    dataBody["PaymentId"] =response.paymentId;
    dataBody["Signature"] =response.signature;
    dataBody["Amount"] =totalamount.toString();
    dataBody["UserIdentifier"] =number;
    var orderdata = {};
    orderdata= dataBody;
    String order = json.encode(orderdata);
    // print(order);
    // ignore: unused_local_variable
    var res = await http.post(Uri.encodeFull(
        'https://brahmiappservice.azurewebsites.net/Payment/CapturePayment'),
        body: order,
        headers: {
          "charset": "utf-8",
          "Content-Type": "application/x-www-form-urlencoded",
          "TenantCode": "c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"
        }
    );
    // print(res.statusCode);
    // print(res.body);
    // print(json.decode(res.body));

    var resBody = {};
    resBody["UserIdentifier"] = _UserIdentifierq;
    resBody["Videos"] = jsonDecode(selectedvideos);
    var user = {};
    user= resBody;
    String bodydata = json.encode(user);
    // print(bodydata);
    // ignore: unused_local_variable
    var resp = await http.post(Uri.encodeFull(
        'https://brahmiappservice.azurewebsites.net/Subscription/SubscribeVideoOrNotes'),
        body:bodydata,
        headers: {
          "Content-Type": "application/json",
          "TenantCode": "c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"
        }
    );
    // print(resp.body);
    // print(resp.statusCode);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    // print('Payment Error');
    Fluttertoast.showToast(
        msg: "Payment Error",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red[300],
        textColor: Colors.white,
        fontSize: 16.0
    );

  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "External Wallet",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red[300],
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  //  Post Data for Filter
  _postFilterData() async{
    final form = _formKey.currentState;
    setState(() {
      isLoading = true;
    });
    form.save();
    Map<String, String> headers = {"Content-type": "application/json", "accept" : "application/json","TenantCode":"c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"};
    var jsons = jsonEncode(<String, String>{"Name": _name, "Topic": _topic, "Subject":_subject});
    // print (jsons);
    var response = await http.post('https://brahmiappservice.azurewebsites.net/Video/Get', headers: headers, body: jsons);
    setState(() {
      isLoading = false;
    });
    // int statusCode = response.statusCode;
    // print(statusCode);
    // print(response);
    // print(response.contentLength);
    // print(response.body);
    // String body = response.body;
    setState((){
      videos=json.decode(response.body);
    });
  }

  _displayDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Filter Videos'),
          content: Container(
              height: 230.0,
              child:SafeArea(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          autocorrect: true,
                          decoration: InputDecoration(
                            labelText: 'Subject',
                          ),
                          onSaved: (String value) {
                            _subject = value;
                          },
                        ),

                        SizedBox(
                          height:10.0,
                        ),

                        TextFormField(
                          autocorrect: true,
                          decoration: InputDecoration(
                            labelText: 'Topic',
                          ),
                          onSaved: (String value) {
                            _topic = value;
                          },
                        ),
                        // DropdownButtonFormField(
                        //   items: classes.map((item){
                        //     return new DropdownMenuItem(
                        //       child:new Text(
                        //         item['Name'],
                        //         style: TextStyle(
                        //             fontSize: 15.0
                        //         ),
                        //       ),
                        //       value:item['Identifier'].toString(),
                        //     );
                        //   }).toList(),
                        //   decoration: InputDecoration(
                        //     labelText: 'Class',
                        //   ),
                        //   onChanged: (String newValue) {
                        //     setState(() {
                        //       a=newValue;
                        //       print(a.toString()) ;
                        //     });
                        //   },
                        //   onSaved: (String item) {
                        //     _class = item;
                        //   },
                        // ),

                        SizedBox(
                          height:25.0,
                        ),

                        TextFormField(
                          autocorrect: true,
                          decoration: InputDecoration(
                            labelText: 'Name',
                          ),
                          onSaved: (String value) {
                            _name = value;
                          },
                        ),
                      ],
                    ),
                  )
              )
          ),
          actions: <Widget>[
            // ignore: deprecated_member_use
            FlatButton(
                color: Colors.deepPurple,
                child: const Text('APPLY'),
                onPressed:(){
                  _postFilterData();
                  Navigator.of(context).pop();
                }
            ),
            // ignore: deprecated_member_use
            FlatButton(
              color: Colors.deepPurple,
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {

        return  isLoading
            ? Center(
          child: CircularProgressIndicator(),
        ):
          Container(
          color: Colors.grey[300],
            child:Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      // ignore: deprecated_member_use
                      RaisedButton(
                      color: Colors.deepPurple,
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.filter_list,color: Colors.white),
                            Text('FILTER',
                              style: TextStyle(
                                  color: Colors.white
                              ),),
                          ],
                        ),
                        onPressed: () {
                          _displayDialog(context);
                        },
                      ),


                      // ignore: deprecated_member_use
                      RaisedButton(
                        color: Colors.deepPurple,
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.add,color: Colors.white),
                            Text('SUBSCRIBE PACKAGE',
                              style: TextStyle(
                                  color: Colors.white
                              ),),
                          ],
                        ),
                        onPressed: () {
                          // Navigator.push(
                          //   context,
                          //   MaterialPageRoute(
                          //     builder: (context) => SubscribeVideo(),
                          //   ),
                          // );
                        },
                      ),
                    ],
                  ),
                ),


                Expanded(
                    child:Container(
                      child:ListView.builder(
                          itemCount: videos.length,
                          itemBuilder: (BuildContext context,int index){
                            return Card(
                              child: Container(
                                height:250,
                                width: 300,
                                child: Column(
                                  children: <Widget>[

                                    Container(
                                   height: 120,
                                    child: Card(
                                    child: Image.network(
                                     '${videos[index]['Resource']['ThumbnailURL']}',
                                       fit: BoxFit.cover,
                                     ),
                                   shape: RoundedRectangleBorder(
                                       borderRadius: BorderRadius.circular(10.0),
                                     ),
                                  elevation: 5,
                                      margin: EdgeInsets.all(10),
    ),
                                    ),
                                    SizedBox(height: 10,),
                                    // Row(
                                    //   //mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    //   children: <Widget>[
                                    //       Image.network(
                                    //         '${videos[index]['Resource']['Thumbnail']}',
                                    //       ),
                                    //     // Expanded(
                                    //     //   child: ChewieListItem(
                                    //     //     videoPlayerController: VideoPlayerController.network(
                                    //     //       '${videos[index]['Resource']['ThumbnailURL']}',
                                    //     //     ),
                                    //     //     looping: true,
                                    //     //
                                    //     //   ),
                                    //     // ),
                                    //   ],
                                    // ),

                                   Container(
                                     color:Colors.white,
                                    padding: const EdgeInsets.all(15),
                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                  Container(
                                                    child: Text(
                                                    'Video: ${videos[index]['Topic']}',
                                                    style: TextStyle(
                                                      //fontSize: 15.0,
                                                      color:Colors.black,
                                                    fontWeight: FontWeight.bold,
                                                    ),
                                                    ),
                                                  ),
                                                  SizedBox(height: 8,),
                                                  Text(
                                                  'Subject: ${videos[index]['Subject']}',
                                                  style: TextStyle(
                                                  color: Colors.black,
                                                    //fontSize: 15.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                  ),
                                                   SizedBox(height: 8,),
                                                Text(
                                                  'Cost: ${videos[index]['Cost']}',
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ],
                                              ),
                                          ),

                                          PopupMenuButton<String>(
                                            onSelected: choiceAction,
                                            itemBuilder: (BuildContext context){
                                              return Menu.choices.map((String choice){
                                                return PopupMenuItem<String>(
                                                  value:'${videos[index]['Identifier']}',
                                                  child: Text(choice),
                                                );
                                              }).toList();
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                    Divider(color: Colors.grey.shade400, indent:0.0, height: 1.0),
                                  ],
                                ),

                              ),
                            );
                          }
                      ),
                    )
                )
              ],
            )
        );
  }
}