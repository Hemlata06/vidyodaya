import 'dart:convert';
//import 'package:filter_list/filter_list.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
//import 'dashboard.dart';
//import 'Menu.dart';
//import 'package:progress_dialog/progress_dialog.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SubscribeVideoAndNotes extends StatefulWidget {
  @override
  _SubscribeVideoAndNotesState createState() => _SubscribeVideoAndNotesState();
}

class _SubscribeVideoAndNotesState extends State<SubscribeVideoAndNotes> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: Text('Subscribe Video & Notes'),
              backgroundColor: Colors.blue[900],
              centerTitle: true,
            ),
            body: Center(
                child:SubscribeVideoAndNotesUser()
            )
        )
    );
  }
}
class SubscribeVideoAndNotesUser extends StatefulWidget {
  SubscribeVideoAndNotesUserState createState() => SubscribeVideoAndNotesUserState();
}

class SubscribeVideoAndNotesUserState extends State {
  Razorpay _razorpay;
  Future<dynamic> futureSubscribe;
  // ignore: deprecated_member_use
  List subscribes = List();
  var name;
  var myclass;
  var duration;
  var discount;
  var cost;
  var isLoading = false;
  var orderid;
  var userIdentifier;
  // ignore: unused_field
  var _userIdentifierq;
  var accesstoken;
  var _accesstokenq;
  // ignore: deprecated_member_use
  List subscribesdata = List();

  Future<dynamic> fetchSubscribe() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _accesstokenq = (prefs.getString('accesstoken')??'');
    setState(() {
      isLoading = true;
    });
    var res = await http.post(Uri.encodeFull(
        'https://brahmiappservice.azurewebsites.net/Subscription/GetSubscriptions'),
      body: {'Identifier': ''},
      headers: {
        "charset": "utf-8",
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json",
        "TenantCode": "c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8",
        "Authorization":_accesstokenq,
      },
    );

    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
    setState(() {
      subscribesdata = json.decode(res.body);
    });
//    setState((){
//      subscribes=json.decode(res.body);
//      name=json.decode(res.body)[0]['Name'];
//      myclass=json.decode(res.body)[0]['Class']['Name'];
//      duration=json.decode(res.body)[0]['Duration'];
//      discount=json.decode(res.body)[0]['Discount'];
//      cost=json.decode(res.body)[0]['Cost'];
//    });
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    futureSubscribe = fetchSubscribe();
    super.initState();
    _razorpay = new Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay = new Razorpay();
    _razorpay.clear(); // Removes all listeners
  }

  var amt = 1 * 100;

  Future<dynamic> openCheckout() async {
    var res = await http.post(Uri.encodeFull(
        'https://brahmiappservice.azurewebsites.net/Payment/CreateOrder?amount=$amt'),
        headers: {
          "charset": "utf-8",
          "Content-Type": "application/x-www-form-urlencoded"
        }
    );
    print(res);
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body)['Attributes']['id']);
    orderid = json.decode(res.body)['Attributes']['id'];
    var options = {
      'key': 'rzp_live_tF2U2M1e2FXGkN',
      'amount': amt, //in the smallest currency sub-unit.
      'name': 'Brahmi',
      'order_id': orderid, // Generate order_id using Orders API
      'description': 'Notes',
      'timeout': 60, // in seconds
      'prefill': {
        'contact': '+919140310165',
        'email': 'riteshseth1990@gmail.com'
      }
    };

    try {
      _razorpay.open(options);
    }
    catch (e) {
      print(e.toString());
    }

    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _userIdentifierq = (prefs.getString('UserIdentifier') ?? '');
    print('Payment Success');
    Fluttertoast.showToast(
        msg: "Payment Successful",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0
    );
    print(response.paymentId);
    print(response.orderId);
    print(response.signature);
    var data = {
      "OrderId": response.orderId,
      "PaymentId": response.paymentId,
      "Signature": response.signature,
      "Amount": amt.toString(),
      "UserIdentifier": _userIdentifierq
    };
    print(data);
    var res = await http.post(Uri.encodeFull(
        'https://brahmiappservice.azurewebsites.net/Payment/CapturePayment'),
        body: data,
        headers: {
          "charset": "utf-8",
          "Content-Type": "application/x-www-form-urlencoded",
          "TenantCode": "c7d0f3f8-37bf-40bf-8ca9-9fb4b577f6b8"
        }
    );
    print(res.statusCode);
    print(res.body);
    print(json.decode(res.body));
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    print('Payment Error');
    Fluttertoast.showToast(
        msg: "Payment Error",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red[300],
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    print('External Wallet');
    Fluttertoast.showToast(
        msg: "External Wallet",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red[300],
        textColor: Colors.white,
        fontSize: 16.0
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : Container(
          child: ListView.builder(
              itemCount: subscribesdata.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  elevation: 5,
                  shape: Border(
                      top: BorderSide(color: Colors.blue[900], width: 5)),
                  child: Container(
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            Container(
                              color: Colors.blue[600],
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                        padding: const EdgeInsets.all(20.0),
                                        child: Column(
                                          children: <Widget>[
                                            Text('${subscribesdata[index]['Name']}', style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white),),
                                          ],
                                        )
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Table(
                              // textDirection: TextDirection.rtl,
                              // defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
                              // border:TableBorder.all(width: 2.0,color: Colors.red),
                                children: [
                                  TableRow(
                                      children: [
                                        Text('Class',
                                          style: TextStyle(fontSize: 17),),
                                        Text('${subscribesdata[index]['Class']['Name']}', style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.blueGrey,
                                            fontWeight: FontWeight.bold),),
                                      ]
                                  ),
                                  TableRow(
                                      children: [
                                        Text('Discount',
                                          style: TextStyle(fontSize: 17),),
                                        Text('${subscribesdata[index]['Discount']}'.toString(),
                                          style: TextStyle(fontSize: 17,
                                              color: Colors.blueGrey,
                                              fontWeight: FontWeight.bold),),
                                      ]
                                  ),
                                  TableRow(
                                      children: [
                                        Text('Duration',
                                          style: TextStyle(fontSize: 17),),
                                        Text('${subscribesdata[index]['Duration']}',
                                          style: TextStyle(fontSize: 17,
                                              color: Colors.blueGrey,
                                              fontWeight: FontWeight.bold),),
                                      ]
                                  ),
                                  TableRow(
                                      children: [
                                        Text('Cost', style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.red[900]),),
                                        Text('${subscribesdata[index]['Cost']}', style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.red[900]),),
                                      ]
                                  ),
                                ]
                            ),
                            Divider(color: Colors.grey.shade400,
                                indent: 0.0,
                                height: 1.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                // ignore: deprecated_member_use
                                RaisedButton(
                                  color: Colors.blue[900],
                                  shape: StadiumBorder(),
                                  child: Row(
                                    children: <Widget>[
                                      Text('Subscribe',
                                        style: TextStyle(
                                            color: Colors.white
                                        ),),
                                    ],
                                  ),
                                  onPressed: () {
                                    openCheckout();
                                  },
                                ),
                              ],
                            )
                          ],
                        )
                    ),
                  ),
                );
              }
          ),
        )
    );
  }
}






